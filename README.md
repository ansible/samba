# Samba

This role configures a password-protected SAMBA share with configurable
path, users, etc., depending on host.

> Samba needs to bind to ports 139 and 445 to function properly.


## Attempt to restrict Samba to only connect via Tailscale

On one particular host I wanted to restrict Samba so it only binds to
the Tailscale interface. My intention was to secure the Samba share to some
extent (this way, any client connecting to this Samba share would need to first
be connected to our Tailscale network).

Setting in `smb.conf`:
```
   interfaces = lo enp37s0
   bind interfaces only = yes
```
results in:
```
$ sudo netstat -plnt | grep smb
tcp     0     0 127.0.0.1:139        0.0.0.0:*    LISTEN     988279/smbd
tcp     0     0 127.0.0.1:445        0.0.0.0:*    LISTEN     988279/smbd
tcp     0     0 192.168.1.102:445    0.0.0.0:*    LISTEN     988279/smbd
tcp     0     0 192.168.1.102:139    0.0.0.0:*    LISTEN     988279/smbd
tcp6    0     0 ::1:139              :::*         LISTEN     988279/smbd
tcp6    0     0 ::1:445              :::*         LISTEN     988279/smbd
```
and predictably connecting to the share using the LAN IP address works,
and connecting using the Tailscale IP fails.

But limiting interfaces to just Tailscale in `smb.conf`:
```
interfaces = lo tailscale0
bind interfaces only = yes
```
results in:
```
$ sudo netstat -plnt | grep smb
tcp     0     0 127.0.0.1:139       0.0.0.0:*     LISTEN     990922/smbd
tcp     0     0 127.0.0.1:445       0.0.0.0:*     LISTEN     990922/smbd
tcp6    0     0 ::1:139             :::*          LISTEN     990922/smbd
tcp6    0     0 ::1:445             :::*          LISTEN     990922/smbd
```
The lack of any Tailscale IPs is glaring.
And as expected, connecting via Tailscale IP fails. And so does connections
via the LAN IP address.

From another Ubuntu server on the Tailnet:
```
$ smbclient -L //100.24.22.20//
do_connect: Connection to 100.24.22.20 failed (Error NT_STATUS_CONNECTION_REFUSED)
```

On the other hand, setting `bind interfaces only = no` allows connections
to Samba via all interfaces, including the LAN IP address or the Tailscale IP address.
```
$ sudo netstat -plnt | grep smb
tcp     0    0 0.0.0.0:139      0.0.0.0:*    LISTEN     1009947/smbd
tcp     0    0 0.0.0.0:445      0.0.0.0:*    LISTEN     1009947/smbd
tcp6    0    0 :::139           :::*         LISTEN     1009947/smbd
tcp6    0    0 :::445           :::*         LISTEN     1009947/smbd
```
and we can successfully connect from a server on the LAN:
```
$ smbclient -L //192.168.1.102//
Password for [WORKGROUP\solarchemist]:

	Sharename       Type      Comment
	---------       ----      -------
	MOUNTS          Disk
	IPC$            IPC       IPC Service (savino server (Samba, Ubuntu))
SMB1 disabled -- no workgroup available
```

So binding Samba to only the regular network interface (`enp37s0` in this case)
works just fine. But binding it to just the Tailscale interface does not work.
Why is that?

What if we use the IP address instead of the interface name:
```
interfaces = lo 100.24.22.20/32
bind interfaces only = yes
```
results in:
```
$ sudo netstat -plnt | grep smb
tcp     0    0 127.0.0.1:139       0.0.0.0:*      LISTEN     993158/smbd
tcp     0    0 127.0.0.1:445       0.0.0.0:*      LISTEN     993158/smbd
tcp6    0    0 ::1:139             :::*           LISTEN     993158/smbd
tcp6    0    0 ::1:445             :::*           LISTEN     993158/smbd
```
and connecting still fails via either method.

+ https://reddit.com/r/Tailscale/comments/qmfcaw/smb_issues_via_tailscale/
+ https://superuser.com/questions/311658/make-a-network-drive-available-over-the-internet/311664#311664

Is Tailscale perhaps acting as a firewall and blocking port 445?
No, that is not the case.

I do not understand why binding Samba to the Tailscale interface fails.

Tailscale client v1.34.0, Samba v4.15.9, Ubuntu 22.04.1.

[I posted a question on StackExchange](https://superuser.com/questions/1757181/binding-samba-to-tailscale-interface-fails-but-works-otherwise).
No relevant issues regarding Samba in the Tailscale issue queue
https://github.com/tailscale/tailscale/issues?q=is%3Aissue+samba

Maybe we should emulate `smb.conf` from this Docker setup:
https://github.com/Niklasthegeek/docker-smb-tailscale
```
[global]
    netbios name = Tailscale-Alpine-Samba %v
    workgroup = WORKGROUP
    server string = Samba Server in Alpine Docker with Tailscale VPN
    security = user
    guest account = nobody
    map to guest = Bad User
    # for security reasons you shouldn't allow versions below SMB2
    server min protocol = SMB2
    # disable printing services
    load printers = no
    printing = bsd
    printcap name = /dev/null
    disable spoolss = yes

[data]
    path = /data
    comment = Shared Folder
    browseable = yes
    read only = no
    guest ok = yes
```


## Links and notes

+ https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html


### Samba tutorials

+ https://linuxize.com/post/how-to-install-and-configure-samba-on-ubuntu-18-04
+ https://www.techrepublic.com/article/how-to-set-up-quick-and-easy-file-sharing-with-samba
+ https://osauthority.com/2018/12/22/install-configure-samba-ubuntu-18-04-lts
+ https://www.linuxfordevices.com/tutorials/linux/linux-samba


### Anonymous (passwordless) Samba shares

+ https://unix.stackexchange.com/questions/619106/how-to-make-this-samba-share-accessible-without-any-user-password-login
+ https://serverfault.com/questions/630631/how-to-make-samba-share-to-not-ask-for-password
